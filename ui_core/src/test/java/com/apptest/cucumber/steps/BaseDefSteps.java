package com.apptest.cucumber.steps;

import com.android.ddmlib.IDevice;
import com.apptest.support.TestEnvironment;
import com.apptest.support.pageobject.ScreenProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.appium.java_client.android.AndroidDriver;


public class BaseDefSteps {

    protected IDevice mDevice;
    protected AndroidDriver mDriver;
    public static final Logger LOG = LoggerFactory.getLogger(BaseDefSteps.class);
    protected ScreenProvider mScreenProvider;


    public BaseDefSteps()  {
        LOG.info("Load device and drivers");
        mDriver = TestEnvironment.getInstance().getDriver();
        mDevice = TestEnvironment.getInstance().getDevice();
        mScreenProvider = new ScreenProvider(mDriver);
    }






}
