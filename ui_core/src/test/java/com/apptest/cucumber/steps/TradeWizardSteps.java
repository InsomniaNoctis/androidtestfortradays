package com.apptest.cucumber.steps;


import com.apptest.support.screens.EventsFilterScreen;
import com.apptest.support.screens.MainScreen;

import org.junit.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class TradeWizardSteps extends BaseDefSteps {

    @And("^Tradays main screen is opened$")
    public void tradaysAppIsOpened() {
        LOG.debug("Tradays main screen is opened");
        MainScreen mainScreen = mScreenProvider.provideScreen(MainScreen.class);
        mainScreen.waitForActive();
    }

    @And("^Events list is not empty on Tradays main screen$")
    public void eventsListIsNotEmptyOnTradaysMainScreen() {
        LOG.debug("Events list is not empty on Tradays main screen");
        MainScreen mainScreen = mScreenProvider.provideScreen(MainScreen.class);
        Assert.assertTrue("Events list are empty!", mainScreen.isEventsListNotEmpty());
    }

    @When("^I open filters$")
    public void iOpenFilters() {
        LOG.debug("I open filters");
        MainScreen mainScreen = mScreenProvider.provideScreen(MainScreen.class);
        mainScreen.clickFilter();
    }

    @Then("^I open first event in sorted list$")
    public void iOpenFirstEventInSortedList() {
        MainScreen mainScreen = mScreenProvider.provideScreen(MainScreen.class);
        mainScreen.selectFirstEvent();

    }


}
