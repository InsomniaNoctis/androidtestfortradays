package com.apptest.cucumber.steps;


import com.apptest.support.screens.EventsFilterScreen;
import org.testng.Assert;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class EventFilterSteps extends BaseDefSteps {

    @Then("^Event filters screen is appeared$")
    public void eventFiltersScreenIsAppeared() {
        LOG.debug("Event filters screen is appeared");
        EventsFilterScreen eventsFilterScreen = mScreenProvider.provideScreen(EventsFilterScreen.class);
        eventsFilterScreen.waitForActive();

    }

    @When("^I filter economic calendar with Importance:\"([^\"]*)\"$")
    public void iFilterEconomicCalendarWithImportance(String value) throws Throwable {
        LOG.debug("I filter economic calendar with Importance:" + value);
        EventsFilterScreen eventsFilterScreen = mScreenProvider.provideScreen(EventsFilterScreen.class);
        eventsFilterScreen.selectCurrentImportance(value);
    }


    @And("^I check that all options are not selected$")
    public void iCheckThatAllOptionsAreNotSelected() {
        LOG.debug("I check that all options are not selected");
        EventsFilterScreen eventsFilterScreen = mScreenProvider.provideScreen(EventsFilterScreen.class);
        eventsFilterScreen.selectAllImportance();
        eventsFilterScreen.selectAllCountries();
        Assert.assertTrue(eventsFilterScreen.areCheckBoxesEmpty(), "Some checkboxes are selected");
    }

    @And("^I filter economic calendar with Country:\"([^\"]*)\"$")
    public void iFilterEconomicCalendarWithCountry(String country) throws Throwable {
        LOG.debug("I filter economic calendar with Country:" + country);
        EventsFilterScreen eventsFilterScreen = mScreenProvider.provideScreen(EventsFilterScreen.class);
        eventsFilterScreen.selectCurrentCountry(country);
    }

    @When("^I back from Event filters screen$")
    public void iBackFromEventFiltersScreen() {
        LOG.debug("I back from Event filters screen");
        EventsFilterScreen eventsFilterScreen = mScreenProvider.provideScreen(EventsFilterScreen.class);
        eventsFilterScreen.clickNavigateUp();
    }


}
