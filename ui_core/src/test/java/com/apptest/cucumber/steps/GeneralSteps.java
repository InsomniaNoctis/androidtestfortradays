package com.apptest.cucumber.steps;

import org.testng.Assert;
import cucumber.api.java.en.Then;

public class GeneralSteps extends BaseDefSteps {


    @Then("^Tradays's Lang is \"([^\"]*)\"$")
    public void tradaysSLangIs(String value) throws Throwable {
        LOG.debug("Tradays's Lang is " + value);
        Assert.assertTrue((mDriver.getCapabilities().getCapability("language")).equals("en"));
    }
}
