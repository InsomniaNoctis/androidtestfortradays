package com.apptest.cucumber.steps;


import com.apptest.support.environment.HistoryEvent;
import com.apptest.support.screens.CurrentEventHistoryTabScreen;
import com.apptest.support.screens.CurrentEventScreen;


import org.testng.Assert;

import java.util.List;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class CurrentEventSteps extends BaseDefSteps {


    @Then("^Current Event screen is appeared$")
    public void currentEventScreenIsAppeared() {
        LOG.debug("Current Event screen is appeared");
        CurrentEventScreen currentEventScreen = mScreenProvider.provideScreen(CurrentEventScreen.class);
        currentEventScreen.waitForActive();
    }

    @And("^Priority is \"([^\"]*)\" on Current Event screen$")
    public void priorityIsOnCurrentEventScreen(String importance) throws Throwable {
        LOG.debug("Priority is " + importance + " on Current Event screen");
        CurrentEventScreen currentEventScreen = mScreenProvider.provideScreen(CurrentEventScreen.class);
        Assert.assertTrue(currentEventScreen.hasImportanceCurrentValue(importance));
    }

    @And("^Country is \"([^\"]*)\" on Current Event screen$")
    public void countryIsOnCurrentEventScreen(String country) throws Throwable {
        LOG.debug("Country is " + country + " on Current Event screen");
        CurrentEventScreen currentEventScreen = mScreenProvider.provideScreen(CurrentEventScreen.class);
        Assert.assertTrue(currentEventScreen.hasCountryCurrentValue(country));
    }

    @When("^I click on tab 'History'$")
    public void iClickOnTabHistory() {
        LOG.debug("I click on tab 'History'");
        CurrentEventScreen currentEventScreen = mScreenProvider.provideScreen(CurrentEventScreen.class);
        currentEventScreen.clickTabHistory();
    }

    @Then("^The history list is opened$")
    public void theHistoryListIsOpened() {
        LOG.debug("The history list is opened'");
        CurrentEventHistoryTabScreen currentEventHistoryTabScreen = mScreenProvider.provideScreen(CurrentEventHistoryTabScreen.class);
        currentEventHistoryTabScreen.waitForActive();
    }

    @When("^I grab last events for (\\d+) months$")
    public void iGrabLastEventsForMonths(int months) throws Exception {
        LOG.debug("I grab last events for  months'");
        CurrentEventHistoryTabScreen currentEventHistoryTabScreen = mScreenProvider.provideScreen(CurrentEventHistoryTabScreen.class);
        List<HistoryEvent> log = currentEventHistoryTabScreen.grabEvents(months);
        Assert.assertNotNull(log, "The events for " + months + " months are not found");
        String leftAlignFormat = "|  %-12s | %-8s | %-10s | %-10s |%n";
        LOG.debug("+---------------+----------+------------+------------+%n");
        LOG.debug("|   Date        |  Actual  |  Forecast  |  Previous  |%n");
        for (HistoryEvent historyEvent : log) {
            LOG.debug(String.format(leftAlignFormat, historyEvent.getDate(), historyEvent.getActual(), historyEvent.getForecast(), historyEvent.getPrevious()));
        }
        LOG.debug("+---------------+----------+------------+------------+%n");
    }
}
