package com.apptest.cucumber.hooks;

import com.android.ddmlib.IDevice;
import com.apptest.support.AdbShell;
import com.apptest.support.TestEnvironment;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterTest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;

public class GlobalCucumberHooks {


    private static final Logger LOG = LoggerFactory.getLogger(GlobalCucumberHooks.class);


    @Before(order = 0)
    public void clearLogs(Scenario scenario) {
        LOG.debug("Clear logs before " + scenario.getName());
        IDevice device = TestEnvironment.getInstance().getDevice();
        AdbShell.clearLogcat(device);
    }

    @After(order = 1)
    public void takeScreenshoot(Scenario scenario) throws IOException {
        if (scenario.isFailed()) {
            LOG.debug("Take a screenshoot after failed " + scenario.getName());
            Allure.addAttachment(scenario.getName(), new ByteArrayInputStream(TestEnvironment.getInstance().takesScreenshot()));
        }
    }

    @After(order = 0)
    public void getLogs(Scenario scenario) throws IOException {
        if (scenario.isFailed()) {
            LOG.debug("Take a adb log after failed " + scenario.getName());
            Path content = Paths.get(TestEnvironment.getInstance().takeAdbLog().getAbsolutePath());
            try (InputStream is = Files.newInputStream(content)) {
                Allure.addAttachment("Adb logs", is);
            }
        }
    }
}
