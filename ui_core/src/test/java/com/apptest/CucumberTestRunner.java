package com.apptest;

import com.apptest.support.TestEnvironment;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.TestNGCucumberRunner;
import cucumber.api.testng.CucumberFeatureWrapper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


@CucumberOptions(
        features = "src/test/resources/features",
        glue = {"com.apptest.cucumber"},
        tags = {"@Test1"},
        plugin = {"json:build/reports/tests/cucumber/cucumber-report.json",
                "io.qameta.allure.cucumberjvm.AllureCucumberJvm"

        })
public class CucumberTestRunner {
    private TestNGCucumberRunner mTestNGCucumberRunner;
    private static final String TAG = CucumberTestRunner.class.getSimpleName();

    @BeforeClass(alwaysRun = true)
    public void setUpClass() throws Exception {
        TestEnvironment.getInstance().setUp();
        mTestNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
    }

    @Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
    public void feature(CucumberFeatureWrapper cucumberFeature) {
        mTestNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
    }

    @DataProvider
    public Object[][] features() {
        return mTestNGCucumberRunner.provideFeatures();
    }

    @AfterClass(alwaysRun = true)
    public void tearDownClass() throws Exception {
        TestEnvironment.getInstance().tearDown();
        mTestNGCucumberRunner.finish();
    }
}