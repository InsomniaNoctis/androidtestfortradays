package com.apptest.support.environment;


public class HistoryEvent {
    private String mDate;
    private String mActual;
    private String mForecast;
    private String mPrevious;

    public HistoryEvent(String date, String actual, String forecast, String previous) {
        this.mDate = date;
        this.mActual = actual;
        this.mForecast = forecast;
        this.mPrevious = previous;
    }


    public HistoryEvent() {

    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String mDate) {
        this.mDate = mDate;
    }

    public String getActual() {
        return mActual;
    }

    public void setActual(String mActual) {
        this.mActual = mActual;
    }

    public String getForecast() {
        return mForecast;
    }

    public void setForecast(String mForecast) {
        this.mForecast = mForecast;
    }

    public String getPrevious() {
        return mPrevious;
    }

    public void setPrevious(String mPrevious) {
        this.mPrevious = mPrevious;
    }
}