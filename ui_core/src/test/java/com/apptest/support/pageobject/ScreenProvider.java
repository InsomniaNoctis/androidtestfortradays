package com.apptest.support.pageobject;

import com.apptest.support.screens.ScreenObject;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import io.appium.java_client.android.AndroidDriver;

public final class ScreenProvider {

    private HashMap<String, ScreenObject> mScreens = new HashMap<>();
    private final AndroidDriver mDriver;

    public ScreenProvider(AndroidDriver driver){
        this.mDriver = driver;
    }

    public <T extends ScreenObject> T provideScreen(Class<T> clazz) {
        final String key = clazz.getName();
        if (mScreens.containsKey(key)){
            return (T) mScreens.get(key);
        } else
        try {
            ScreenObject screen = null;
            try {
                Constructor<?> constructor = clazz.getConstructor(AndroidDriver.class);
                screen = (ScreenObject) constructor.newInstance(mDriver);
            } catch (NoSuchMethodException  | InvocationTargetException e) {
                throw new RuntimeException("Faile to get instance class for" + clazz + " because " + e);
            }
            mScreens.put(key,screen);
            return (T) screen;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
