package com.apptest.support;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.CollectingOutputReceiver;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class AdbShell {

    private static final Logger LOG = LoggerFactory.getLogger(AdbShell.class);

    private AdbShell() {

    }

    public static void clearApp(IDevice device, String packageName){
        String shellCmd = String.format("pm clear %s", packageName);
        CollectingOutputReceiver outputReceiver = new CollectingOutputReceiver();
        try {
            device.executeShellCommand(shellCmd, outputReceiver, 3, TimeUnit.SECONDS);
            LOG.info("Success");
        } catch (TimeoutException
                | AdbCommandRejectedException
                | ShellCommandUnresponsiveException
                | IOException e) {
            LOG.debug(e.getMessage());
        }
    }

    public static void clearLogcat(IDevice device){
        String shellCmd = String.format("logcat -c");
        CollectingOutputReceiver outputReceiver = new CollectingOutputReceiver();
        try {
            device.executeShellCommand(shellCmd, outputReceiver, 3, TimeUnit.SECONDS);
            LOG.info("Success");
        } catch (TimeoutException
                | AdbCommandRejectedException
                | ShellCommandUnresponsiveException
                | IOException e) {
            LOG.debug(e.getMessage());
        }
    }
}
