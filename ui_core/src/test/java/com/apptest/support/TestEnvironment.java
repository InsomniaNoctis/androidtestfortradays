package com.apptest.support;

import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.IDevice;
import com.apptest.support.utils.NetworkUtils;

import io.appium.java_client.android.AndroidDriver;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Properties;

import java.io.File;
import java.net.URL;

import static com.android.ddmlib.IDevice.PROP_BUILD_VERSION;
import static com.android.ddmlib.IDevice.PROP_DEVICE_MODEL;

public final class TestEnvironment {

    private static final Logger LOG = LoggerFactory.getLogger(TestEnvironment.class);
    private static TestEnvironment INSTANCE = new TestEnvironment();

    private static final String PROPERTIES_FILE_NAME = "gradle.properties";
    private static final String PROP_APP_LOCALE = "pAppLocale";
    private static final String PROP_APP_LANGUAGE = "pAppLanguage";
    private static final String PROP_APK_PACKAGE_NAME = "pTradaysPackageName";
    private static final String PROP_APK_START_ACTIVITY = "pTradaysStartActivity";
    private static final String PROP_APK_PATH = "pTradaysApkPath";
    private static final String PROP_APPIUM_SERVER_PACKAGE_NAME = "pAppiumServerPackageName";
    private static final String PROP_APPIUM_SERVER_TEST_PACKAGE_NAME = "pAppiumServerTestPackageName";
    private static final String PROP_APPIUM_PORT = "pAppiumPort";
    private static final String PROP_APPIUM_JS_PATH = "pAppiumJsPath";
    private static final String PROP_NODE_JS_PATH = "pNodeJsPath";
    private AndroidDebugBridge mAdb = null;
    private IDevice mDevice = null;
    private AppiumDriverLocalService mAppiumService = null;
    private Properties mProperties = null;

    public AndroidDriver mDriver;

    private TestEnvironment() {

    }

    public static TestEnvironment getInstance() {
        return INSTANCE;
    }


    private static Properties readProperties() throws IOException {
        LOG.debug("Get properties from gradle.properties!");
        Properties prop = new Properties();
        InputStream is = null;
        try {
            is = new FileInputStream(new File(getRoot(), PROPERTIES_FILE_NAME));
            prop.load(is);
        } finally {
            if (is != null) {
                is.close();
            }
        }
        return prop;
    }

    public Properties getProperties() throws IOException {
        if (mProperties == null) {
            mProperties = readProperties();
        }
        return mProperties;
    }


    public void setUp() throws Exception {
        int appiumPort = Integer.parseInt(getProperties().getProperty(PROP_APPIUM_PORT));
        if(!NetworkUtils.checkIfServerIsRunnning(appiumPort)) {
            startAppium(appiumPort);
        } else {
            System.out.println("Appium Server already is running on the port - " + appiumPort);
        }
        IDevice device = getDevice();
        AdbShell.clearLogcat(device);



    }

    private static File getRoot() {
        File curDir = new File(System.getProperty("user.dir"));
        return curDir.getParentFile();
    }

    private static File getProjectRoot() {
        File curDir = new File(System.getProperty("user.dir"));
        return curDir.getAbsoluteFile();

    }

    public void tearDown() throws Exception {
        String appiumServer = getProperties().getProperty(PROP_APPIUM_SERVER_PACKAGE_NAME);
        String appiumServerTest = getProperties().getProperty(PROP_APPIUM_SERVER_TEST_PACKAGE_NAME);
        IDevice device = getDevice();
        quitDriver();
        stopAppium();
        AdbShell.clearApp(device, appiumServer);
        AdbShell.clearApp(device, appiumServerTest);
        device.uninstallPackage(appiumServer);
        device.uninstallPackage(appiumServerTest);
    }


    public IDevice getDevice() {
        if (mDevice == null) {
            LOG.debug("Get list of devices");
            AndroidDebugBridge.init(false);
            mAdb = AndroidDebugBridge.createBridge();
            waitDevicesList(mAdb);
            IDevice devices[] = mAdb.getDevices();
            if (devices == null) {
                throw new RuntimeException("No device available");
            } else {
                mDevice = devices[0];
            }
        }
        return mDevice;
    }

    private void waitDevicesList(AndroidDebugBridge bridge) {
        LOG.debug("Wait Device");
        int count = 0;
        while (bridge.hasInitialDeviceList() == false) {
            try {
                Thread.sleep(500);
                count++;
            } catch (InterruptedException e) {
            }
            if (count > 60) {
                System.err.print("");
                break;
            }
        }
    }

    public AndroidDriver getDriver() {

        if (mDriver == null) {
            try {
                LOG.debug("Get new Android");
                String appiumPort = getProperties().getProperty(PROP_APPIUM_PORT);
                DesiredCapabilities caps = new DesiredCapabilities();
                caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, getDevice().getProperty(PROP_BUILD_VERSION));
                caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
                caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, getProperties().getProperty(PROP_APK_PACKAGE_NAME));
                caps.setCapability(MobileCapabilityType.DEVICE_NAME, getDevice().getProperty(PROP_DEVICE_MODEL));
                caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, getProperties().getProperty(PROP_APK_START_ACTIVITY));
                caps.setCapability(MobileCapabilityType.APP, new File(getProperties().getProperty(PROP_APK_PATH)).getCanonicalPath());
                caps.setCapability("language", getProperties().getProperty(PROP_APP_LANGUAGE));
                caps.setCapability("locale", getProperties().getProperty(PROP_APP_LOCALE));
                caps.setCapability("clearDeviceLogsOnStart", true);
                caps.setCapability("newCommandTimeout", 5000);
                mDriver = new AndroidDriver(new URL("http://127.0.0.1:"+appiumPort+"/wd/hub"), caps);
                LOG.debug("Driver has started successfully!");

            } catch (Exception e) {
                throw new RuntimeException("Failed to get Android Driver!");
            }

        }
        return mDriver;
    }

    public void quitDriver() {
        if (mDriver != null) {
            LOG.debug("Quit driver");
            mDriver.quit();
        }
    }

    public void startAppium(int appiumPort) throws IOException {

        AppiumServiceBuilder builder = new AppiumServiceBuilder();
        builder.withIPAddress("127.0.0.1");
        builder.usingPort(appiumPort);
        builder.withAppiumJS(new File(getProperties().getProperty(PROP_APPIUM_JS_PATH)));
        builder.usingDriverExecutable(new File(getProperties().getProperty(PROP_NODE_JS_PATH)));
        builder.withLogFile(new File("appium.log"));
        mAppiumService = AppiumDriverLocalService.buildService(builder);


        mAppiumService.start();

    }

    public void stopAppium() {
        if (mAppiumService!=null) {
            mAppiumService.stop();
        }
    }


    public byte[] takesScreenshot() throws IOException {
        return ((TakesScreenshot) mDriver).getScreenshotAs(OutputType.BYTES);

    }

    public File takeAdbLog() throws FileNotFoundException {
        List<LogEntry> logEntries = mDriver.manage().logs().get("logcat").getAll();
        File logFile = new File(getProjectRoot(), "logs/adb_log.txt");
        PrintWriter logWriter = new PrintWriter(logFile);
        logWriter.println(logEntries);
        logWriter.flush();
        logWriter.close();
        LOG.info(mDriver.getSessionId() + ": Save device log. DONE.");
        return logFile;
    }


}
