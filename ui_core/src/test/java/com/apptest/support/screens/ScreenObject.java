package com.apptest.support.screens;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import static java.time.Duration.ofSeconds;
import static org.openqa.selenium.support.PageFactory.*;

public abstract class ScreenObject {

    protected AndroidDriver<AndroidElement> mDriver;
    private int defaultLook = 20;

    public ScreenObject(AndroidDriver mDriver) {
        this.mDriver = mDriver;
    }

    public void waitForActive() {
        initElements(new AppiumFieldDecorator(mDriver, ofSeconds(defaultLook)), this);
    }

    public AndroidElement scrollDownByText(String text){
      return mDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+ text + "\").instance(0))");
    }



    public boolean waitForSelected(AndroidElement androidElement, int timeLimitInSeconds){
        boolean isElementPresent;
        try{
            WebDriverWait wait = new WebDriverWait(mDriver, timeLimitInSeconds);
            wait.until(ExpectedConditions.elementToBeSelected(androidElement));
            isElementPresent = androidElement.isDisplayed();
            return isElementPresent;
        }catch(Exception e){
            isElementPresent = false;
            System.out.println(e.getMessage());
            return isElementPresent;
        }
    }

}