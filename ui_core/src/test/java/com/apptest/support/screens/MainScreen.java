package com.apptest.support.screens;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import java.util.List;

public class MainScreen extends ScreenObject{

    @AndroidFindBy(id = "menu_filter")
    private AndroidElement mMenuFilter;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView")
    private List<AndroidElement> mEvents;

    public MainScreen(AndroidDriver driver) {
        super(driver);
    }

    public void clickFilter(){
        mMenuFilter.click();
    }

    public boolean isEventsListNotEmpty(){
        if (mEvents.isEmpty()){
            return false;
        } else {
            return true;
        }
    }

    public void selectFirstEvent(){
        mEvents.get(0).click();
    }



}
