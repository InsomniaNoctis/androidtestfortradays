package com.apptest.support.screens;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class CurrentEventScreen extends ScreenObject {


    @AndroidFindBy(id = "event_importance")
    private AndroidElement mEventImportance;

    @AndroidFindBy(id = "country_name")
    private AndroidElement mCountryName;

    @AndroidFindBy(id = "tab_overview")
    private AndroidElement mTabOverview;

    @AndroidFindBy(id = "tab_chart")
    private AndroidElement mTabChart;

    @AndroidFindBy(id = "tab_history")
    private AndroidElement mTabHistory;


    public CurrentEventScreen(AndroidDriver mDriver) {
        super(mDriver);
    }

    public boolean hasImportanceCurrentValue(String value) {
        return mEventImportance.getText().equals(value.toUpperCase());
    }

    public boolean hasCountryCurrentValue(String value) {
        return mCountryName.getText().equals(value);
    }

    public void clickTabHistory() {
        mTabHistory.click();
    }
}
