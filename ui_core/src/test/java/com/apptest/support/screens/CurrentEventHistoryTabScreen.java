package com.apptest.support.screens;


import com.apptest.support.environment.HistoryEvent;
import com.apptest.support.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;


public class CurrentEventHistoryTabScreen extends ScreenObject {

    @AndroidFindBy(id = "event_importance")
    private AndroidElement mEventImportance;

    @AndroidFindBy(id = "country_name")
    private AndroidElement mCountryName;

    @AndroidFindBy(id = "tab_overview")
    private AndroidElement mTabOverview;

    @AndroidFindBy(id = "tab_chart")
    private AndroidElement mTabChart;

    @AndroidFindBy(id = "tab_history")
    private AndroidElement mTabHistory;

    @AndroidFindBy(id = "head")
    private AndroidElement mHead;

    @AndroidFindBy(id = "content")
    private AndroidElement mContent;

    @AndroidFindBy(id = "time")
    private List<AndroidElement> mTime;

    @AndroidFindBy(id = "actual")
    private List<AndroidElement> mActual;

    @AndroidFindBy(id = "forecast")
    private List<AndroidElement> mForecast;

    @AndroidFindBy(id = "previous")
    private List<AndroidElement> mPrevious;


    public CurrentEventHistoryTabScreen(AndroidDriver mDriver) {
        super(mDriver);

    }

    public List grabEvents(int months) throws Exception {
        String endDateMonth = null;
        int endDateYear = 0;
        List<HistoryEvent> historyEvents = new ArrayList<>();
        if (!mTime.isEmpty() && mTime.size() < months) {
            for (int i = 2; i < mTime.size(); i++) {
                String Date = mTime.get(i).getText();
                String Actual = mActual.get(i).getText();
                String Forecast = mForecast.get(i).getText();
                String Previous = mPrevious.get(i).getText();
                historyEvents.add(new HistoryEvent(Date, Actual, Forecast, Previous));
            }
            endDateMonth = DateUtils.getMonth(DateUtils.StringToDate(mTime.get(2).getText()));
            endDateYear = DateUtils.getYear(DateUtils.StringToDate(mTime.get(2).getText())) - 1;
            scrollDownByText(endDateMonth + " " + endDateYear);
        }
        for (int i = 0; i < months; i++) {
            String Date = mTime.get(i).getText();
            if (Date.contains(endDateMonth + " " + endDateYear) && months==12) {
                break;
            }
            String Actual = mActual.get(i).getText();
            String Forecast = mForecast.get(i).getText();
            String Previous = mPrevious.get(i).getText();
            HistoryEvent historyEvent = new HistoryEvent(Date, Actual, Forecast, Previous);
            if (!historyEvents.contains(historyEvent)) {
                historyEvents.add(historyEvent);
            }
        }
        return historyEvents;
    }
}
