package com.apptest.support.screens;

import java.util.List;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class EventsFilterScreen extends ScreenObject {


    @AndroidFindBy(xpath = "//android.widget.LinearLayout/" +
            "android.widget.TextView[starts-with(@text,'IMPORTANCE')]" +
            "/following-sibling::android.widget.Button")
    private AndroidElement mSelectAllImportance;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout/" +
            "android.widget.TextView[starts-with(@text,'COUNTRY')]" +
            "/following-sibling::android.widget.Button")
    private AndroidElement mSelectAllCountry;

    @AndroidFindBy(id = "check")
    private List<AndroidElement> mCheckBox;

    @AndroidFindBy(uiAutomator = "new UiSelector().description(\"Navigate up\")")
    private AndroidElement mNavigateUp;


    public EventsFilterScreen(AndroidDriver driver) {
        super(driver);
    }

    public void selectAllCountries() {
        mSelectAllCountry.click();
    }

    public void clickNavigateUp() {
        mNavigateUp.click();
    }

    public void selectAllImportance() {
        mSelectAllImportance.click();
    }

    public void selectCurrentImportance(String name) {
        AndroidElement currentImportance = findCurrentImportance(name);
        currentImportance.click();
    }

    public void selectCurrentCountry(String country) {
        AndroidElement currentCountry = findCurrentCountry(country);
        currentCountry.click();
    }

    public boolean areCheckBoxesEmpty() {
        if (!waitForSelected(mCheckBox.get(0), 5)) {
            for (AndroidElement m : mCheckBox
            ) {
                if (m.isSelected()) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean areCheckBoxesChecked() {
        if (waitForSelected(mCheckBox.get(0), 5)) {
            for (AndroidElement m : mCheckBox
            ) {
                if (!m.isSelected()) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }


    private AndroidElement findCurrentImportance(String name) {
        return mDriver.findElementByAndroidUIAutomator("new UiSelector().textContains(\"" + name + "\").instance(0)");
    }

    private AndroidElement findCurrentCountry(String country) {
        return scrollDownByText(country);
    }

}
