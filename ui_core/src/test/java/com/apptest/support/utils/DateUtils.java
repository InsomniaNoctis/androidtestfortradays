package com.apptest.support.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public final class DateUtils {

    public static Date StringToDate(String stringDate) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("d MMM yyyy", Locale.ENGLISH);
        Date date = formatter.parse(stringDate);
        return date;
    }


    public static int getYear(Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        return Integer.parseInt(dateFormat.format(date));
    }
    public static String getMonth(Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);
        return dateFormat.format(date);
    }
}

