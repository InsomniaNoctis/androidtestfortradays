@Test1

  Feature: Check Event in App

    Background:
      Given Tradays main screen is opened
      And Events list is not empty on Tradays main screen
      And Tradays's Lang is "en"

    Scenario Outline: User checks event at sorted list
      When I open filters
      Then Event filters screen is appeared
      And I check that all options are not selected
      When I filter economic calendar with Importance:"<Importance>"
      And I filter economic calendar with Country:"<Country>"
      When I back from Event filters screen
      Then Tradays main screen is opened
      And Events list is not empty on Tradays main screen
      When I open first event in sorted list
      Then Current Event screen is appeared
      And Priority is "<Importance>" on Current Event screen
      And Country is "<Country>" on Current Event screen
      When I click on tab 'History'
      Then The history list is opened
      And I grab last events for 12 months




      Examples:
      | Importance | Country |
      | Medium     | Switzerland |

