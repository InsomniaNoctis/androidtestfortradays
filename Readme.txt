1.Устанавливаем Java JDK не выше и не ниже 8 версии. 

2.Устанавливаем Android Studio (https://developer.android.com/studio/index.html)

3.Во время установки выберете пункт Custom и запомните путь к Android SDK.

4.После установки идем в "Мой компьютер, свойства системы, дополнительные параметры системы, переменные среды"

5.Нажимаем создать, имя ANDROID_HOME, значение - путь до Android SDK, нажимаем ОК. 
Удалим несуществующие пути к Sdk и JAVA_HOME. Создадим JAVA_HOME, значение - путь до Java JDK, например: C:\Program Files\Java\jdk1.8.0_221

6.Далее выбираем переменную Path, Изменить.
В список добавляем 3 папки tools, platform-tools и build-tools/<версия build-tools - желательно 28.0.3> через: Создать, Обзор и выбираем нужную папку. Ок, Ок.
7.Запускаем cmd, проверяем 2 программы adb и aapt на наличие.

8. Убедиться, что установлен git. Поставить любой удобный для вас клиент Git.

10. Устанавливаем Appium Desktop 1.15 (https://github.com/appium/appium-desktop/releases/tag/v1.15.1) и Node.js не ниже 10 версии (https://nodejs.org/ru/download/)

11. Клонируем проект по ссылке: https://gitlab.com/InsomniaNoctis/androidtestfortradays.git

12. Запускаем Appium и стартуем сервер на порте 4725.

13. Подключаем любой девайс (или запускаем эмулятор).

14. Открываем проект ui_core из склонированного репозитория в Android Studio. Для синхронизации gradle нажмите Refresh Gradle Project на вкладке справа. Скачаются необходимые библиотеки.



Путь к Apk в gradle.properties:
pTradaysApkPath = ../Rss/output/apk/release/net.metaquotes.economiccalendar_39.apk

Запуск консольного аппиума (пути к нужным файлам):

pNodeJsPath = D:\\Program Files\\nodejs\\node.exe
pAppiumJsPath = C:\\Users\\<User>\\AppData\\Local\\Programs\\Appium\\resources\\app\\node_modules\\appium\\build\\lib\\main.js



ЗАПУСК ТЕСТОВ:

команда в терминале Android Studio: gradlew --info --stacktrace clean :ui_core:test

Либо через CucumberTestRunner.java (правой кнопкой мыши Run 'CucumberTestRunner')

Отчёт аллюра можно получить, выполнив в терминале таску: gradlew allureReport
